<?php

require_once('animals.php');
require_once('frog.php');
require_once('ape.php');

$object = new animals("Shaun");

echo "Nama Hewan : " . $object->name . "<br>";
echo "Jumlah Kaki : " . $object->leg . "<br>";
echo "Darah Dingin : " . $object->cold_blooded . "<br><br>";

$object2 = new frog("Buduk");
echo "Nama Hewan :" . $object2->name . "<br>";
echo "Jumlah Kaki : " . $object2->leg . "<br>";
echo "Darah Dingin : " . $object2->cold_blooded . "<br>";
$object2->jump() . "<br><br>";

$object3 = new ape("Kera Sakti");
echo "Nama Hewan :" . $object3->name . "<br>";
echo "Jumlah Kaki : " . $object3->leg . "<br>";
echo "Darah Dingin : " . $object3->cold_blooded . "<br>";
$object3->yell();