Soal 1 Membuat Database
 
CREATE DATABASE myshop;
 

Soal 2 Membuat Table di Dalam Database
 

 CREATE TABLE users(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null,
    email varchar(255) NOT null,
    password varchar(255) NOT null
);
    
CREATE TABLE categories(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null
);
    
CREATE TABLE items(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    description varchar(255),
    price int(8),
    stock int(8),
    category_id int(8),
	FOREIGN KEY(category_id) REFERENCES categories(id)
);

Soal 3 Memasukkan Data pada Table
 

insert data tabel users
INSERT INTO users(name,email,password) VALUES ("john Doe","john@doe.com","john123");
INSERT INTO users(name,email,password) VALUES ("Jane Doe","jane@doe.com","jenita123");

insert data tabel categories
INSERT INTO categories(name) VALUES ("gadget"), ("Cloth"), ("men"), ("women"), ("branded");

insert data tabel items
INSERT INTO items(name,description,price,stock,category_id) VALUES ("Sumsang b50","hape keren dari merek sumsang","4000000","100","1");
INSERT INTO items(name,description,price,stock,category_id) VALUES ("Uniklooh","bajuk keren dari brand ternama","500000","50","2");
INSERT INTO items(name,description,price,stock,category_id) VALUES ("IMHO Watch","jam tangan anak yang jujur banget","2000000","10","1");


 
Soal 4 Mengambil Data dari Database
 

a. Mengambil data users
 
select name, email from users;

b. Mengambil data items
 
SELECT * FROM items WHERE price > 1000000;

SELECT * FROM items WHERE name LIKE '%sang%';
 

c. Menampilkan data items join dengan kategori
 

SELECT items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name from items INNER JOIN categories on items.category_id = categories.id;

 
Soal 5 Mengubah Data dari Database

UPDATE items set price = 2500000 WHERE name = 'sumsang b50';