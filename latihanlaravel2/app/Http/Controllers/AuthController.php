<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('register');
    }

    //  public function welcome(Request $request)
    //  {
    //      $firstname = $request->input('firstname');
    //     $lastname = $request->input('lastname');
           
    //     return view('welcome',['firstname'=>$firstname,'lastname'=>$lastname]);
    //  }


    public function welcome(Request $request)
    {
        $fname = $request->input('fname');
        $lname = $request->input('lname');
           
        return view('welcome',compact('fname', 'lname'));
    }

    // public function welcome(Request $request)
    // {
    //     dd($request->all());
    // }


}
