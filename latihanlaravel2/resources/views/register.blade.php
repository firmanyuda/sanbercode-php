@extends('layout.master')
@section('title')
    Halaman Form
@endsection
@section('content')
    <form method="post" action="/welcome">
        @csrf

        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <label>First name:</label><br>
            <input type="text" name="fname"><br><br>
        <label>Last name:</label><br>
            <input type="text" name="lname"><br><br>
        <label>Gender:</label>
            <input type="radio" name="gender">Male<br>
            <input type="radio" name="gender">Female<br>
            <input type="radio" name="gender">Other<br><br>
        <label>Nationality: </label>
            <select name="ngrasal" size="1">
                <option value="id">Indonesia</option>
                <option value="my">Malaysia</option>
                <option value="sg">Singapore</option>
            </select><br><br>
        <label>Language Spoken: </label><br>
            <input type="checkbox" name="lspoken">Indonesia<br>
            <input type="checkbox" name="lspoken">English<br>
            <input type="checkbox" name="lspoken">Other<br><br>
        <label>Bio:</label><br>        
            <textarea name="bio" rows="5" cols="100"></textarea><br><br>
        <input type="submit" value="Sign Up" >

    </form>
@endsection



