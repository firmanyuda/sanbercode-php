<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@homepage');
Route::get('/register', 'AuthController@form');
Route::post('/welcome', 'AuthController@welcome');

// Route::get('/master', function(){
//     return view('layout.master');
// });

Route::get('/table', function(){
    return view('layout/table');
});

Route::get('/data-table', function(){
    return view('layout/data-table');
});

//CRUD Cast

// CREATE
// Route menuju ke form pembuatan tambah cast
Route::get('/cast/create', 'CastController@create');

// Menyimpan data ke database
Route::post('/cast', 'CastController@store');

// Read
// Route untuk menampilkan semua data pemain
Route::get('/cast', 'CastController@index');
// Route untuk detail cast
Route::get('/cast/{cast_id}', 'CastController@show');

// Update
// Route untuk mengarah ke halaman form edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//untuk update data berdasarkan id
route::put('/cast/{cast_id}', 'CastController@update');

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');




    