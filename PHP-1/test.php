<?php
function ubah_huruf($string){
//kode di sini
$shiftBy = 1;
$alphabet = 'abcdefghijklmnopqrstuvwxyz';

$string = substr($alphabet, $shiftBy) . substr($alphabet, 0, $shiftBy);
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>